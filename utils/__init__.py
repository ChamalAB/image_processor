# standard library
import os
import pickle

# third party
import face_recognition
import cv2
import numpy as np
from PIL import Image


def createBlankImage(height,width,color=(0,0,0)):
    """
    Creates a blank image
    color only supports 3 channels
    Input will have to adapt to either
    RGB - Pillow
    BGR - OpenCv
    """
    # create empty array
    data = np.zeros((height,width,3),dtype=np.uint8)
    data[:] = tuple(color)
    return data


def placeTextBL(frame, text, font_scale=0.6,font_color=(0,0,255), font_weight=2):
    """
    Places text from bottom left, towards up
    
    frame - OpenCv image object (numpy.ndarray)
    text  - list of strings (first element will appear on top)
    font_scale - Font size relative to base size
    font_color - tuple in BGR format
    font_weight - Font weight
    """

    # loop over the info tuples and draw them on our frame
    for i,text_elem in enumerate(reversed(text)):
        cv2.putText(frame, str(text_elem), (10, frame.shape[0] - ((i * 20) + 20)),
            cv2.FONT_HERSHEY_SIMPLEX, font_scale, font_color, font_weight)
        
    return frame
    

def gray_frame(frame):
	return cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)



def draw_boxes(frame, boxes, color=(255,0,0), thickness=2):
    """
    Supports box format from OpenCV multiDict
    """
    for (x,y,w,h) in boxes:
        frame = cv2.rectangle(frame,(x,y),(x+w,y+h),color,thickness)

    return frame



def draw_boxes_2(frame, boxes, color=(255,0,0), thickness=2):
    """
    Supports box format from OpenCV DNN module
    """
    for (startX,startY,endX,endY) in boxes:
        frame = cv2.rectangle(frame,(startX,startY),(endX,endY),color,thickness)

    return frame



def BoxCSS(image,coordinates,color=(0,255,0),thickness=2):
    """
    CSS style coordinates for box is
    (top,right,bottom,left)
    
    top = y_max
    bottom = y_min
    right = x_max
    left = x_min
    """
    return cv2.rectangle(image,
                         (coordinates[3],coordinates[2]),
                         (coordinates[1],coordinates[0]),
                         color, thickness)


def CropCSS(image,coordinates):
    """
    Takes in CSS coordinates and crops
    CSS style coordinates for box is
    (top,right,bottom,left)

    top = y_max = endY
    bottom = y_min = startY
    right = x_max = endX
    left = x_min = startX
    
    numpy crop
    [top:bottom , left:right]
    """
    return image[coordinates[0]:coordinates[2],coordinates[3]:coordinates[1]]


def CropStandard(image,coordinates):
    """
    Standard style coordinates for box is
    
    
    top = y_max = endY
    bottom = y_min = startY
    right = x_max = endX
    left = x_min = startX

    numpy crop
    [top:bottom , left:right]
    """
    (startX,startY,endX,endY) = coordinates

    return image[startY:endY,startX:endX]


def CropDetectMultiScale(image,coordinates):
    """
    Designed for Cv2.detectMultiScale module only
    Not tested for anything else
    """
    (x, y, w, h) = coordinates
    r = max(w, h) / 2
    centerx = x + w / 2
    centery = y + h / 2
    nx = int(centerx - r)
    ny = int(centery - r)
    nr = int(r * 2)
    
    return image[ny:ny+nr, nx:nx+nr]


def GrabCroppedFaces(IMAGE_DIR = 'known_faces', view=False, upsamples = 1):
    images = []
    for file in os.listdir(IMAGE_DIR):
        if file.lower().endswith(('.jpg','.png','.bmp')):
            images.append(np.asarray(Image.open(os.path.join(IMAGE_DIR,file))))

    cropped_faces = list()

    # iter over all images
    for image in images:
        # get faces /bounding boxes
        # get faces
        for box in face_recognition.api.face_locations(image,number_of_times_to_upsample=upsamples):
            # view all cropped images
            if view:
                _ = Image.fromarray(CropCSS(image,box)).show()
            cropped_faces.append(CropCSS(image,box))
    
    return cropped_faces


def GrabFaceEncodings(IMAGE_DIR = 'known_faces'):
    images = []
    for file in os.listdir(IMAGE_DIR):
        if file.lower().endswith(('.jpg','.png','.bmp')):
            images.append(np.asarray(Image.open(os.path.join(IMAGE_DIR,file))))
    
    encodings = list()
    # iter over all images
    for image in images:
        # get faces /bounding boxes
        # get faces
        encodings += face_recognition.api.face_encodings(image)
                      
    return encodings



def check_flag_file(filename):
    if os.path.isfile(filename):
        os.remove(filename)
        return True
    else:
        return False


def input_get_boolean(prompt):
    while True:
        text = input(prompt)
        if text.lower() in ['y','yes','yeah','yup']:
            return True
        elif text.lower() in ['n','no','nope','nah']:
            return False
        else:
            print('Unknown command please try again')
            continue