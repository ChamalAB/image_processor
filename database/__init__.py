import mysql.connector
import json


class databaseConnection:
    def __init__(self, **kwargs):
        self.db = mysql.connector.connect(**kwargs)
    
    
    def execute(self,sql,args,fetch=True):
        result = None
        with self.db.cursor(dictionary=True,buffered=True) as cursor:
            cursor.execute(sql,args)
            self.db.commit()
            if fetch:
                result = cursor.fetchall()
        return result
    
    
    def close(self):
        self.db.close()
        
        
    def __del__(self):
        self.close()
        
        
class svsDB (databaseConnection):
    
    def updateEvent(self,obj):
        if obj['type'] == 'arrived':
            self.arrive(obj)
        elif obj['type'] == 'left':
            self.left(obj)
        elif obj['type'] == 'recognition':
            self.recognize(obj)
        else:
            raise Exception('Unsupported event type')
    
    def arrive(self,obj):
        # insert movement
        self._insertMovement(obj['time'],obj['cameraid'],obj['tempID'])
        # insert remain
        self._postRemain(obj['time'],obj['cameraid'],obj['remainCount']+1)
        # insert event
        self._postEvent(json.dumps(obj))
    
    
    def left(self,obj):
        # update movement
        self._updateMovementLeft(obj['time'],obj['cameraid'],obj['tempID'])
        # insert remain
        self._postRemain(obj['time'],obj['cameraid'],obj['remainCount'])
        # insert event
        self._postEvent(json.dumps(obj))
    
    
    def recognize(self,obj):
        # update movement
        self._updateMovementName(obj['name'],obj['cameraid'],obj['tempID'])
        # insert event
        self._postEvent(json.dumps(obj))
    
    
    def _insertMovement(self,timeIn,cameraId,tempId):
        sql = "INSERT INTO `svs2`.`movement` (`cameraid`, `timein`, `tempid`) VALUES (%s, %s, %s)"
        args = (cameraId,timeIn,tempId)
        self.execute(sql,args,fetch=False)
    
    
    def _updateMovementLeft(self,timeOut,cameraId,tempId):
        sql = "UPDATE `svs2`.`movement` SET `timeout` = %s where (`tempid` = %s and `cameraid` = %s) ORDER BY `idmovement` DESC LIMIT 1"
        args = (timeOut,tempId,cameraId)
        self.execute(sql,args,fetch=False)
    
    
    def _updateMovementName(self,name,cameraId,tempId):
        sql = "UPDATE `svs2`.`movement` SET `name` = %s where (`tempid` = %s and `cameraid` = %s) ORDER BY `idmovement` DESC LIMIT 1"
        args = (name,tempId,cameraId)
        self.execute(sql,args,fetch=False)
    
    
    def _postRemain(self,_time,CameraId,remain):
        sql = "INSERT INTO `svs2`.`remain` (`cameraid`, `time`, `remain`) VALUES (%s, %s, %s)"
        args = (CameraId,_time,remain)
        self.execute(sql,args,fetch=False)
    
    
    def _postEvent(self,event):
        sql = "INSERT INTO `svs2`.`event` (`stringified`) VALUES (%s)"
        args = (event,)
        self.execute(sql,args,fetch=False)