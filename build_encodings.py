# standard library
import os
import pickle

# third party
# load configs
from config import (KNOWN_FACES_DIR,ENCODINGS_DIR)

# user defined
from utils import BoxCSS, CropCSS, GrabCroppedFaces, GrabFaceEncodings


# delete existing encodings
if os.path.isdir(ENCODINGS_DIR):
    
    print('ENCODINGS_DIR dir found. clearing data..')
    for file in os.listdir(ENCODINGS_DIR):
        if os.path.isdir(os.path.join(ENCODINGS_DIR,file)):
            os.rmdir(os.path.join(ENCODINGS_DIR,file))
        else:
            os.remove(os.path.join(ENCODINGS_DIR,file))
            
else:
    print('ENCODINGS_DIR dir not found. creating one..')
    os.mkdir(ENCODINGS_DIR)


# browse to known_faces dir
if os.path.isdir(KNOWN_FACES_DIR):
    print('KNOWN_FACES_DIR dir found..')
    for file in os.listdir(KNOWN_FACES_DIR):
        # check of file is dir
        if os.path.isdir(os.path.join(KNOWN_FACES_DIR,file)):
            print(file,'named dir found..')
            data = {
                'name':file,
                'encodings':GrabFaceEncodings(os.path.join(KNOWN_FACES_DIR,file))
            }
            print('{} encodings found for {}'.format(len(data['encodings']),file))
            
            if len(data['encodings']) > 0:
                print('serializing {}.encodings'.format(os.path.join(ENCODINGS_DIR,file)))
                with open('{}.encodings'.format(os.path.join(ENCODINGS_DIR,file)),'wb') as dumpfile:
                    pickle.dump(data,dumpfile)
                    
            
    
else:
    raise Exception('KNOWN_FACES_DIR dir is not found')
