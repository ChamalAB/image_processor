import os
import time

import cv2
from imutils import resize

# user defined
from VideoProcessor.VideoStream import VideoStream
from  utils import input_get_boolean

# grab known_faces dir
from config import (KNOWN_FACES_DIR,ENCODINGS_DIR,cam_settings,selected_profile,flag_file_content)


def get_person():
    # get name of the person
    name = input('please enter the name: ')

    # if dir not available create one
    if not os.path.isdir(os.path.join(KNOWN_FACES_DIR,name)):
        print('dir not found. creating...')
        os.mkdir(os.path.join(KNOWN_FACES_DIR,name))

    cam = VideoStream(url=cam_settings[selected_profile]['src'])
    cam.start()

    cv2.namedWindow("Capture")

    while True:
        frame = cam.read()
        resized = resize(frame,width=600)
        cv2.imshow("Capture", resized)

        k = cv2.waitKey(1)
        if k%256 == 27:
            # ESC pressed
            print("Escape hit, closing...")
            break
        elif k%256 == 32:
            # SPACE pressed
            img_name = "{}_{}.png".format(name,int(time.time()))
            cv2.imwrite(os.path.join(KNOWN_FACES_DIR,name,img_name), frame)
            print("{} written!".format(img_name))

    cam.stop()
    cv2.destroyAllWindows()


# look for KNOWN_FACES_DIR
if os.path.isdir(KNOWN_FACES_DIR):
    print('KNOWN_FACES_DIR dir found...')
else:
    print('KNOWN_FACES_DIR dir not found. Creating...')
    os.mkdir(KNOWN_FACES_DIR)

# allow capture many faces
while True:
    get_person()
    if input_get_boolean('Capture another person? (Yes/No) :'):
        continue
    else:
        break

if input_get_boolean('Rebuild encodings? (Yes/No) :'):
    import build_encodings
else:
    print('skipping encoding')

if input_get_boolean('Prompt image processor to reload? (Yes/No) :'):
    with open('LOAD_FACES.flag','w') as file:
        print('placing LOAD_FACES.flag file...')
        file.write(flag_file_content)

print('script complete....')
