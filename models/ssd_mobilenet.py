# Single shot detector (mobilenet)
import os

import numpy as np
import cv2
import imutils


# models dir absolute path
ssd_spec_path = os.path.join(os.path.dirname(__file__),'ssd_models')

caffe_protxt_file = os.path.join(ssd_spec_path,'MobileNetSSD_deploy.prototxt')
caffe_trained_model = os.path.join(ssd_spec_path,'MobileNetSSD_deploy.caffemodel')

confidence_target = 0.2

# SSD mobilenet pre-trained classes
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
    "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
    "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
    "sofa", "train", "tvmonitor"]

# we only interested in detecting persons
CONSIDER = set(["person"])

# initialize net
net = cv2.dnn.readNetFromCaffe(caffe_protxt_file, caffe_trained_model)



def get_ssd_persons(frame):
    boxes = []
    (h, w) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 0.007843, (300, 300), 127.5)

    net.setInput(blob)
    detections = net.forward()

    for i in np.arange(0, detections.shape[2]):
        confidence = detections[0, 0, i, 2]

        if confidence > confidence_target:
            idx = int(detections[0, 0, i, 1])

            if CLASSES[idx] in CONSIDER:
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")

                boxes.append((startX, startY, endX, endY))

    return boxes





