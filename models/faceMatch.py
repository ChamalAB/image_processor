# standard library
import os
import pickle

# third party
import face_recognition
import cv2
import numpy as np



class faceMatch:
    def __init__(self,activated=True,encoding_dir='encodings'):
        """
        class for simple image recognition
        
        activated is set for False, faceMatch() will return 'Recognition disabled'
        
        this class loads encodings from disk & provides a name if 
        search encoding is provided
        """
        self.activated = activated
        self.encoding_dir = encoding_dir
        self.face_encodings = {}
    
    
    def loadEncodings(self):
        """
        load encodings
        """
        if self.activated:
            # reset old data
            self.face_encodings = {}
            print('loading face encodings')
            # search for path and raise Exception if not exists
            if self.activated:
                if not os.path.isdir(self.encoding_dir):
                    raise Exception('Cannot open',self.encoding_dir)
                    
            # located .encodings files
            for file in os.listdir(self.encoding_dir):
                if file.endswith('.encodings'):
                    print('found:',os.path.join(self.encoding_dir,file))
                    data = self._load_encoding(os.path.join(self.encoding_dir,file))
                    self.face_encodings[data['name']] = data['encodings'] 
        else:
            return
    
    
    def faceMatch(self, encoding):
        """
        provices a name if the face match
        
        encoding: numpy.ndarray
        """
        if self.activated:
            to_search = face_recognition.api.face_encodings(encoding)
            
            if len(to_search) == 1:
                for key, value in self.face_encodings.items():
                    results = face_recognition.api.compare_faces(value,to_search[0])
                    if sum(results):
                        return key
                # no match found return None
                return None
            else:
                # this is an error case, return None
                return None
        else:
            return 'Recognition disabled'
        
    
    def _load_encoding(self,path):
        with open(path,'rb') as file:
            return pickle.load(file)