from collections import OrderedDict, deque
import time



class personTracker:
    """
    Used to track movements and recognitions and 
    produce events
    
    produced events:
    1. In Event
    2. Out Event
    3. Recognition Event
    
    public methods:
    __init__(camaraID)              # need to set the camera ID
    update(CentroidTracker objects) # extracts Objects id
    recognized(objectID)            # returns a name if recognized else return None
    setName(objectID, name)         # associate a name to objectID
    getUpdates()                    # release a queue(deque object) of updates

    private methods:
    _deRegister(ObjectID)           # remove ID, name
    _enqueue(object)                # enqueue an event
    """
    def __init__(self, cameraid='area_01'):
        self.prev_frame = list()   # stores previous state
        self.cameraid = cameraid
        self.names = {} # dict to save objectID:name
        self.events = deque() # queue to hold objects
        
        
    def update(self,Objects):
        if isinstance(Objects, OrderedDict):
            Objects = Objects.keys()
            
        
        current_frame = list() # temp storage current state (somewhat...)
        arrived_frame = list() # temp storage for newly arrived
        
        for objectID in Objects:
            if objectID in self.prev_frame:
                # person remain
                
                # remove person from frame to get a 
                # list of people left at the end
                self.prev_frame.remove(objectID)
                
                # append to current frame
                current_frame.append(objectID)
                
            else:
                # a new person has arrived
                arrived_frame.append(objectID)
                
        # people who left are here
        #if self.prev_frame: print(str(self.prev_frame) + ' left')
        for left in self.prev_frame:
            self._enqueue({
                'type' : 'left',
                'cameraid' : self.cameraid,
                'tempID' : left,
                'time': int(time.time()),
                'remain': current_frame,
                'remainCount': len(current_frame)
            })
            
        # people arrived are here
        #if arrived_frame: print(str(arrived_frame) + ' arrived')
        for arrived in arrived_frame:
            self._enqueue({
                'type' : 'arrived',
                'cameraid' : self.cameraid,
                'tempID' : arrived,
                'time': int(time.time()),
                'remain': current_frame,
                'remainCount': len(current_frame)
            })
            
        # people who remained are here
        #if current_frame: print(str(current_frame) + ' remained')
        
        self.prev_frame = current_frame + arrived_frame
        
        
    def recognized(self,ObjectID):
        # need to fire an event !
        return self.names.get(ObjectID,None)
    
    
    def setName(self,ObjectID, name):
        self.names[ObjectID] = name
        self._enqueue({
            'type' : 'recognition',
            'cameraid' : self.cameraid,
            'tempID' : ObjectID,
            'name' : name,
            'time': int(time.time()),
        })
        
        
    def getUpdates(self):
        return self.events
    
    
    def _deRegister(self, ObjectID):
        del self.names[ObjectID]
        
    
    def _enqueue(self,event):
        self.events.append(event)