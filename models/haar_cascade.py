# Contains haarcascade based recognition models

import numpy as np
import cv2
import imutils
import os


# models dir absolute path
haarcascade_spec_path = os.path.join(os.path.dirname(__file__),'hc_models')

# face recognition settings
face_scale_factor = 1.3
face_min_neighbors = 5
face_classifier_path = os.path.join(haarcascade_spec_path,'haarcascade_frontalface_default.xml')
face_cascade = cv2.CascadeClassifier(face_classifier_path)

# full body recognition settings
body_scale_factor = 1.3
body_min_neighbors = 5
body_classifier_path = os.path.join(haarcascade_spec_path,'haarcascade_fullbody.xml')
body_cascade = cv2.CascadeClassifier(body_classifier_path)


def get_haar_faces(frame):
    return face_cascade.detectMultiScale(frame, face_scale_factor, face_min_neighbors)


def get_haar_bodies(frame):
    return body_cascade.detectMultiScale(frame, body_scale_factor, body_min_neighbors)