from collections import OrderedDict

def calculateCentroid(startX, startY, endX, endY):
    cX = int((startX + endX) / 2.0)
    cY = int((startY + endY) / 2.0)
    return (cX, cY)


class traceCentroid:
    """
    Used to map centroids with bounding boxes
    """
    def __init__(self):
        self.data = {}
        
        
    def update(self,objects,persons):
        """
        objects : objects from CentroidTracker
        perons : objects from person detector
        """
        person_boxes = []
        person_centroid = []
        # calculate centroids for persons
        for person in persons:
            person_centroid.append(calculateCentroid(*person))
            person_boxes.append(person)
            
        self.data = {}
        # iterate over objects(class:CentroidTracker)
        for Id, Cen in objects.items():
            try:
                box = person_boxes[person_centroid.index(tuple(Cen))]
                self.data[Id] = {
                    'box': box,
                    'centroid': Cen
                }
            except ValueError:
                # sometimes persons will not contain a detection but the 
                # tracker will continue to track a person for few more 
                # frames.
                pass
    
    
    def getBox(self,objectID):
        """
        deliver bounding box when the objectID
        is provided
        """
        try:
            return self.data[objectID]['box']
        except KeyError:
            return None