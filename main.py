# standard
import socket
import signal
import sys
import os

# third-party
import cv2
import imagezmq

# user-defined
from VideoProcessor import VideoProcessor
from utils import check_flag_file
from database import svsDB


# load configs
from config import (cam_settings,
selected_profile,
enable_preview_window,
enable_imageZMQ,
imageZMQ_connect_to,
ENABLE_DB,
DB_CREDS)




# image zmq server settings
if enable_imageZMQ:
    sender = imagezmq.ImageSender(connect_to=imageZMQ_connect_to,REQ_REP=False)
    rpi_name = socket.gethostname() # send RPi hostname with each image
    

# setup signal handler
state = True

def signal_handler(sig, frame):
    print('Pressed Ctrl+C!. Exiting')
    global state
    state = False

signal.signal(signal.SIGINT, signal_handler)


# initiate Video process
process = VideoProcessor(**cam_settings[selected_profile])
process.start()

# initialize database connection
if ENABLE_DB:
    database = svsDB(**DB_CREDS)
else:
    database = None


while state:
    # check if new encodings are present
    if check_flag_file('LOAD_FACES.flag'):
        print('LOAD_FACES prompted. loading faces')
        process.loadFaces()

    events = process.getUpdates()
    frame = process.getFrame()

    # process events
    for i in range(len(events)):
        event = events.popleft()
        print(event)
        
        if database:
            database.updateEvent(event)
        


    if enable_preview_window:
        # Display the resulting frame
        cv2.imshow('frame',frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    if enable_imageZMQ:
        sender.send_image(rpi_name, frame)


process.stop()
cv2.destroyAllWindows()