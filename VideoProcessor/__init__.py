# standard


# third-party
import cv2
# from imutils.video import VideoStream
from imutils import resize

# user defined
from .centroidtracker import CentroidTracker
from .VideoStream import VideoStream
from models import get_haar_faces,get_ssd_persons,traceCentroid,personTracker,faceMatch
from utils import draw_boxes, draw_boxes_2, gray_frame, CropDetectMultiScale, CropStandard



class VideoProcessor:
    def __init__(self, src=0, resize=None, framecap=None, cameraid='area_01'):
        self.cap = VideoStream(url=src, name=cameraid)
        self.resize = resize
        self.framecap = framecap
        self.cameraid = cameraid



    def start(self):
        """
        Starts the video capture
        """
        self.loadConfig()

        # start stream
        self.cap.start()

        # create centroid tracker
        self.ct = CentroidTracker()

        # create trace Centroid object
        self.cboxes = traceCentroid()

        # create person tracker that produces events
        self.tracker = personTracker(cameraid=self.cameraid)

        # create face track instance
        self.faceMatch = faceMatch()
        self.faceMatch.loadEncodings()



    def stop(self):
        self.cap.stop()




    def getFrame(self):
        """
        gets the latest processed frame
        if preview is not availble the raw frame is delievred
        """
        return self.frame



    def getUpdates(self):
        """
        Gives out any enqueued updates

        main method to capture frame and processing
        """
        self.frame = self.cap.read()

        # resize if configured
        if self.resize:
            self.frame = resize(self.frame, width=self.resize)


        # get persons
        persons = get_ssd_persons(self.frame)
        self.frame = draw_boxes_2(self.frame,persons,color=(0,255,0))
        self.frame = draw_boxes(self.frame,get_haar_faces(self.frame))
        objects = self.ct.update(persons)
        self.cboxes.update(objects, persons)
        self.tracker.update(objects)


        # place centroid and text
        for (object_id, centroid) in objects.items():
            box = self.cboxes.getBox(object_id)
            name = self.tracker.recognized(object_id)

            if not name:
                # proceed to recognition
                if box:
                    # crop person
                    cropped = CropStandard(self.frame,box)
                    # get face info
                    faces = get_haar_faces(cropped)
                    # ideally there should be only one face !!
                    
                    if len(faces) == 1:
                        # crop face
                        face = CropDetectMultiScale(cropped,faces[0])
                        # send to image recognition
                        name = self.faceMatch.faceMatch(face)
                        if name:
                            self.tracker.setName(object_id, name)

            # check if name is available (this if statement was placed here because
            # inside previous statement name might get resolved
            if name:
                text = 'ID {}: {}'.format(object_id,name)
            else:
                text = 'ID {}'.format(object_id)


            cv2.putText(self.frame, text, (centroid[0] - 10, centroid[1] - 10),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            cv2.circle(self.frame, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)

        return self.tracker.getUpdates()

        

    def loadFaces(self):
        """
        Initializes face encodings
        This is useful to re-load face encodings in runtime

        """
        self.faceMatch.loadEncodings()



    def loadConfig(self):
        """
        Initializes configs
        This is useful to re-load configs in runtime
        """
        pass