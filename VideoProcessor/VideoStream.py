from utils import createBlankImage, placeTextBL
from threading import Thread
import cv2
import time


class VideoStream:
    """
    Designed to handle network based RTSP streams
    - can handle disconnections
    """
    def __init__(self,url,wait=5,attempts=3,name='camera 01'):
        self.url = url
        self.wait = wait
        self.attempts = attempts
        self.name = name
        
        
        # to stop streaming set this to false
        self.state = True

        # intialize attemp acount
        self.attempt_count = 0


        # Generate intial frame 
        self.frame = createBlankImage(500,500,color=(0,0,0))
        self.frame = placeTextBL(self.frame,['%s : initializing..' % self.name])



    def start(self):
        # start the thread to read frames from the video stream
        self.thread = Thread(target=self._startLoop, args=())
        self.thread.daemon = True
        self.thread.start()


    def _startLoop(self):
        try:
            while self.state:
                print("Opening '%s'" % self.url)
                self.cap = cv2.VideoCapture(self.url)

                self._stream()
                self.cap.release()

                # check again whether state has chenged before continuing
                if not self.state:
                    break

                # Start counter
                self.attempt_count += 1
                if self.attempt_count>self.attempts:
                    self.state = False
                else:
                    print("Restart attampt %d of %d.waiting %d seconds before reconnect" % (self.attempt_count,self.attempts,self.wait))
                    # write message on frame
                    self.frame = placeTextBL(self.frame,['%s : re-connecting..' % self.name])

                    # breakable wait loop
                    for second in range(self.wait):
                        if self.state:
                            time.sleep(1)
                        else:
                            # requested to stop the program
                            break

        except Exception as e:
            print("Exception occurred:",str(e))
            print("Restarting loop")
            self._startLoop()
        finally:
            print("Exiting loop")


    def _stream(self):
        while (self.cap.isOpened() and self.state):

            # if a capture is successful reset counter
            self.attempt_count = 0

            ret, frame = self.cap.read()

            if not ret:
                print("Connection failed..")
                break

            if frame is None:
                print("Invalid frame received")
                break
            else:
                self.frame = frame


    def read(self):
        return self.frame


    def stop(self):
        self.state = False


    def getState(self):
        return self.state


    def join(self):
        self.thread.join()

