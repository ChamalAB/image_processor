import os
import sys

# third-party
import mysql.connector

# user defined
from database import databaseConnection
from  utils import input_get_boolean

# load configs
from config import (DB_CREDS)

if not input_get_boolean('Are you sure you want re-initialize the Database? All data will be lost (Yes/No) :'):
    print('Operation cancelled...')
    sys.exit(0)


print('Re-initializing database...')
with open(os.path.join('database','initdb.sql'),'r') as file:
    script = file.read()
    db = mysql.connector.connect(host=DB_CREDS['host'], user=DB_CREDS['user'], password=DB_CREDS['password'])
    with db.cursor() as cursor:
        cursor.execute('DROP SCHEMA svs2',())
        db.commit()
        cursor.executemany(script,())
        db.commit()
        
    db.close()
print('Done...')